// import * as wasm from "hello-world";

import { greet } from "greet";
import alert from "alert";

greet();
alert("hey guy! Are you going abroad?!", "zenity");

import("wasm-pkg").then((wasm) => {
  const sum = wasm.add(1, 20);
  console.log(sum);
});
