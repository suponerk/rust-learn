use rand::Rng;
use std::io;

fn main() {
    let secret = rand::thread_rng().gen_range(1..10);
    println!("Secret is {}", secret);
    println!("Please type something to stdin");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You typed: {}", guess);
}
