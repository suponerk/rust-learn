use std::process::Command;

struct Shell{}
impl Shell {
    
    pub fn run(command: &str, arguments: &str) {   
        println!("RUST RUN SHELL COMMAND '{} {}':", command, arguments);     
        let output = Command::new(command)
                            .arg(arguments)
                            .output()
                            .expect("Failed to execute process");
    
        // println!("status: {}", output.status);
        // println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
        // println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
        println!("{}", String::from_utf8_lossy(&output.stdout));
    
    
        assert!(output.status.success());
    }

}

fn main() {
    // Shell::run("ls", "-la");
    // Shell::run("ps", "-e");
    Shell::run("touch", "newfile.txt");
    Shell::run("ls", "-la");
}